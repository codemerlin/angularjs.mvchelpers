﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using FluentValidation;
using FluentValidation.Attributes;
using FluentValidation.Results;

namespace Angularjs.MvcHelpers.Api
{
    public class Response
    {
        public dynamic Model { get; set; }
        public dynamic[] Errors { get; set; }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ValidateInputsAttribute : ActionFilterAttribute
    {
        private static readonly IValidatorFactory ValidatorFactory = new AttributedValidatorFactory();

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            var errors = new List<dynamic>();
            foreach (KeyValuePair<string, object> arg in actionContext.ActionArguments.Where(a => a.Value != null))
            {
                var argType = arg.Value.GetType();
                IValidator validator = ValidatorFactory.GetValidator(argType);
                if (validator != null)
                {
                    var validationResult = validator.Validate(arg.Value);
                    foreach (ValidationFailure error in validationResult.Errors)
                    {
                        errors.Add(new { Error = error, ElementId = argType.Name + "-" + error.PropertyName });
                    }
                }
            }
            if (errors.Any())
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest
                                                                              , errors,
                                                                              actionContext.ControllerContext
                                                                                           .Configuration.Formatters
                                                                                           .JsonFormatter);

                //actionContext.Result = new JsonResult()
                //{
                //    Data = response
                //};
            }
        }
    }
}