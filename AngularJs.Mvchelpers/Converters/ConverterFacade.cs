﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using Angularjs.MvcHelpers.Mvc;
using FluentValidation;
using FluentValidation.Attributes;
using FluentValidation.Internal;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Converters
{
    public static class ConverterFacade
    {
        public static IDictionary<string, object> Convert<TModel, TValue>(Expression<Func<TModel, TValue>> expression, TModel actualModel)
        {
            var proInfo = typeof(TModel).GetProperty(ExpressionHelper.GetExpressionText(expression));

            return GetAttributes(typeof(TModel), proInfo);

        }

        private static IDictionary<string, object> GetAttributes(Type modelType, PropertyInfo propInfo)
        {

            IValidatorFactory ValidatorFactory = new AttributedValidatorFactory();
            //TODO: to make IoC config easier we need to determine a way to not have to use
            //      named instances to get the appropriate validator
            IValidator val = ValidatorFactory.GetValidator(modelType);

            IDictionary<string, object> result = new Dictionary<string, object>();

            val.ToList().ForEach(rule =>
                          {
                              var property = rule as PropertyRule;
                              if (property != null && property.PropertyName.ToLower() == propInfo.Name.ToLower())
                              {
                                  rule.Validators.ToList()
                                      .ForEach(propertyValidator =>
                                          result.Add(ConverterFactory.Instance.GetConverter(propertyValidator).Convert()));
                              }
                          });



            return result;
        }
    }
}