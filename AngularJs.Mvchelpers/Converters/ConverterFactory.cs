﻿using System.Linq;
using System.Reflection;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Converters
{
    public class ConverterFactory : IConverterFactory
    {
        private ConverterFactory()
        {

        }

        public static IConverterFactory Instance
        {
            get
            {
                return new ConverterFactory();
            }
        }


        public IConverter GetConverter(IPropertyValidator validatorType)
        {
            switch (validatorType.GetType().Name)
            {
                case "GreaterThanValidator":
                    return new GreaterThanConverter(validatorType);
                case "LessThanOrEqualValidator":
                    return new LessThanConverter(validatorType);
                case "RegularExpressionValidator":
                    return new RegularExpressionConverter(validatorType);
                default:
                    return new DefaultConverter(validatorType);
            }
        }
    }
}