﻿using System;
using System.Collections.Generic;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Converters
{
    public class GreaterThanConverter : IConverter<GreaterThanValidator>
    {
        public GreaterThanConverter(IPropertyValidator propertyValidator)
        {
            if (!(propertyValidator is GreaterThanValidator))
            {
                throw new ApplicationException("Wrong Class Being instantiated");
            }
            Validator = propertyValidator as GreaterThanValidator;

        }
        public GreaterThanValidator Validator { get; private set; }


        public KeyValuePair<string, object> Convert()
        {
            return new KeyValuePair<string, object>("min", Validator.ValueToCompare);
        }
    }
}