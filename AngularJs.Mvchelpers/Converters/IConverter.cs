﻿using System.Collections.Generic;

namespace Angularjs.MvcHelpers.Converters
{
    public interface IConverter
    {
        KeyValuePair<string, object> Convert();

    }
    public interface IConverter<out TValidatorType> : IConverter
    {
        TValidatorType Validator { get; }


    }
}