﻿using System;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Converters
{
    public interface IConverterFactory
    {
        IConverter GetConverter(IPropertyValidator validatorType);
    }
}