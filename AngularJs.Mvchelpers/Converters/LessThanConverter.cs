﻿using System;
using System.Collections.Generic;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Converters
{
    public class LessThanConverter : IConverter<LessThanValidator>
    {
        public LessThanConverter(IPropertyValidator propertyValidator)
        {
            if (!(propertyValidator is LessThanValidator))
            {
                throw new ApplicationException("Wrong Class Being instantiated");
            }
            Validator = propertyValidator as LessThanValidator;

        }
        public LessThanValidator Validator { get; private set; }


        public KeyValuePair<string, object> Convert()
        {
           return new KeyValuePair<string,object>("max", Validator.MemberToCompare);
        }
    }
}