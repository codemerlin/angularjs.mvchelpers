﻿using System;
using System.Collections.Generic;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Converters
{
    public class RegularExpressionConverter : IConverter<RegularExpressionValidator>
    {
        public RegularExpressionConverter(IPropertyValidator propertyValidator)
        {
            if (!(propertyValidator is RegularExpressionValidator))
            {
                throw new ApplicationException("Wrong Class Being instantiated");
            }
            Validator = propertyValidator as RegularExpressionValidator;

        }
        public RegularExpressionValidator Validator { get; private set; }


        public KeyValuePair<string, object> Convert()
        {
            return new KeyValuePair<string, object>("ng-pattern", Validator.Expression);
        }
    }
}