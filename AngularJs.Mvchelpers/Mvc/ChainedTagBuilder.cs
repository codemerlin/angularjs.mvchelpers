﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Angularjs.MvcHelpers.Mvc
{
    public class ChainedTagBuilder
    {
        public ChainedTagBuilder(string tagName)
            : this(new TagBuilder(tagName))
        {
        }

        public ChainedTagBuilder(TagBuilder tagBuilder)
        {
            this.TagBuilder = tagBuilder;
        }

        public TagBuilder TagBuilder
        { get; private set; }


        public IDictionary<string, string> Attributes
        {
            get
            {
                return this.TagBuilder.Attributes;
            }
        }

        public ChainedTagBuilder Attribute(string key, string value)
        {
            this.TagBuilder.Attributes.Add(key, value);
            return this;
        }

        public string IdAttributeDotReplacement
        {
            get { return this.TagBuilder.IdAttributeDotReplacement; }
            set { this.TagBuilder.IdAttributeDotReplacement = value; }
        }

        public string Html
        {
            get { return this.TagBuilder.InnerHtml; }
            set { this.TagBuilder.InnerHtml = value; }
        }

        public string TagName
        {
            get { return this.TagBuilder.TagName; }
        }

        public ChainedTagBuilder AddCssClass(string value)
        {
            this.TagBuilder.AddCssClass(value);
            return this;
        }

        public ChainedTagBuilder SetHtml(string innerHtml)
        {
            this.TagBuilder.InnerHtml = innerHtml;
            return this;
        }

        public ChainedTagBuilder MergeAttributes(string key, string value, bool replaceExisting)
        {
            this.TagBuilder.MergeAttribute(key, value, replaceExisting);
            return this;
        }

        public ChainedTagBuilder MergeAttributes<TKey, TValue>(IDictionary<TKey, TValue> attributes)
        {
            this.TagBuilder.MergeAttributes(attributes);
            return this;
        }

        public ChainedTagBuilder MergeAttributes<TKey, TValue>(IDictionary<TKey, TValue> attributes, bool replaceExisting)
        {
            this.TagBuilder.MergeAttributes(attributes, replaceExisting);
            return this;
        }

        public ChainedTagBuilder SetInnerText(string innerText)
        {
            this.TagBuilder.SetInnerText(innerText);
            return this;
        }

        public string ToString(TagRenderMode renderMode)
        {
            return this.TagBuilder.ToString(renderMode);
        }

        public override string ToString()
        {
            return this.TagBuilder.ToString();
        }
    }
}