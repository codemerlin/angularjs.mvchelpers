﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using FluentValidation;
using FluentValidation.Attributes;
using FluentValidation.Internal;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Mvc
{
    //str - extensions for Html
    public static class HtmlExtensions
    {
        private enum ValidationTypes
        {
            GreatThan,
            LessThan,
            LessThanOrEqualTo,
            RegularExpression
        }
        private class ValidationConfig
        {
            public ValidationTypes ValidationType { get; set; }

            public object ValueToCompare { get; set; }

            public string Expression { get; set; }
        }

        public static MvcHtmlString NgValidationMessageFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string formName)
        {
            MemberExpression memberExpression = expression.Body as MemberExpression;
            PropertyInfo propInfo = memberExpression.Member as PropertyInfo;

            string elementId = memberExpression.Expression.Type.BaseType.Name + "-" + propInfo.Name + "-ValidationMessage";
            return new MvcHtmlString("<span class=\"error validation-message\" id=\"" + elementId + "\" ng-show=\"!" + formName + "." + propInfo.Name + ".$valid\">This is invalid</span>");
        }

        public static MvcHtmlString NgRadioButtonListFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> selectList, bool renderNgInit = false)
        {
            throw new NotImplementedException();
        }

        public static MvcHtmlString NgDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            return NgDisplayFor(html, expression, "span");
        }

        public static MvcHtmlString NgDisplayFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string wrapperTag, bool renderNgInit = false)
        {
            //var id = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(ExpressionHelper.GetExpressionText(expression));

            IDictionary<string, object> htmlAttributes = BuildHtmlAttributes(html, expression, renderNgInit);
            string ngModelName = htmlAttributes.Where(a => a.Key == "data-ng-model").First().Value.ToString();

            StringBuilder attributeString = new StringBuilder();
            foreach (KeyValuePair<string, object> attribute in htmlAttributes)
            {
                attributeString.Append(attribute.Key + "=\"" + attribute.Value + "\" ");
            }

            return MvcHtmlString.Create(string.Format("<{0} {1}>{2}</{0}>", wrapperTag, attributeString, "{{" + ngModelName + "}}"));
        }

        public static MvcHtmlString NgListBoxFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, IEnumerable<SelectListItem> selectList, bool renderNgInit = false)
        {
            IDictionary<string, object> htmlAttributes = BuildHtmlAttributes(html, expression, renderNgInit);
            
            return html.ListBoxFor(expression, selectList, htmlAttributes);
        }

        public static MvcHtmlString NgDropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, bool renderNgInit = false)
        {
            IDictionary<string, object> htmlAttributes = BuildHtmlAttributes(html, expression, renderNgInit);

            return html.DropDownListFor(expression, selectList, htmlAttributes);
        }

        public static MvcHtmlString NgTextBoxFor<TModel, TValue>(this HtmlHelper<TModel> html,
                                                                 Expression<Func<TModel, TValue>> expression, bool renderNgInit = false)
        {
            PropertyInfo propInfo = ((MemberExpression) expression.Body).Member as PropertyInfo;

            //define the model
            IDictionary<string, object> htmlAttributes = BuildHtmlAttributes(html, expression, renderNgInit);

            //if this is a number make it a number input
            List<string> numTypes = new List<string>() { "Int16", "Int32", "Int64" };

            if (numTypes.Contains(propInfo.PropertyType.Name))
            {
                htmlAttributes.Add("type", "number");
                htmlAttributes.Add("ng-pattern", "/\\d+/");
                //in order to get angular to place nice with html 5 required must be present
                htmlAttributes.Add("required", "");
            }

            var result = html.TextBoxFor(expression, htmlAttributes);

            //some hacks
            string hacks = result.ToString();
            hacks = hacks.Replace("proxy-validity=\"\"", "proxy-validity"); //cleaning proxy-validation tag
            hacks = hacks.Replace("required=\"\"", "required"); //cleaning required tag
            result = new MvcHtmlString(hacks);
            return result;
        }

        private static IDictionary<string, object> BuildHtmlAttributes<TModel, TValue>(HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, bool renderNgInit = false)
        {
            MemberExpression memberExpression = expression.Body as MemberExpression;
            PropertyInfo propInfo = memberExpression.Member as PropertyInfo;
            List<ValidationConfig> configs = GetValidationConfigs(memberExpression.Expression.Type, propInfo);
            IDictionary<string, object> result = new Dictionary<string, object>();

            string elementName = memberExpression.Expression.Type.BaseType.Name + "." + propInfo.Name;
            result.Add("data-ng-model", elementName);
            result.Add("id", elementName.Replace('.','-'));
            result.Add("name", elementName.Replace('.', '-'));

            if (renderNgInit)
            {
                Func<TModel, TValue> method = expression.Compile();
                TValue value = method(html.ViewData.Model);
                result.Add("data-ng-init", elementName + " = '" + value + "'");
            }

            foreach (var vc in configs)
            {
                switch (vc.ValidationType)
                {
                    case ValidationTypes.RegularExpression:
                        result.Add("ng-pattern", "/" + vc.Expression + "/");
                        break;

                    case ValidationTypes.GreatThan:
                        result.Add("min", vc.ValueToCompare);
                        break;

                    case ValidationTypes.LessThan:
                        result.Add("max", vc.ValueToCompare);
                        break;
                }
            }
            return result;
        }

        private static List<ValidationConfig> GetValidationConfigs(Type modelType, PropertyInfo propInfo)
        {
            
            IValidatorFactory ValidatorFactory = new AttributedValidatorFactory();

            List<ValidationConfig> result = new List<ValidationConfig>();

            //TODO: to make IoC config easier we need to determine a way to not have to use
            //      named instances to get the appropriate validator
            IValidator val = ValidatorFactory.GetValidator(modelType);
            var rules = val.ToList();

            foreach(var rule in rules)
            {
                var validators = rule.Validators.ToList();
                var property = rule as PropertyRule;

                //make sure we only get the rules that apply to this property!
                if (property.PropertyName.ToLower() == propInfo.Name.ToLower())
                {
                    foreach (var propertyValidator in validators)
                    {
                        switch (propertyValidator.GetType().Name)
                        {
                            case "GreaterThanValidator":
                                GreaterThanValidator gtv = propertyValidator as GreaterThanValidator;
                                result.Add(new ValidationConfig()
                                               {
                                                   ValidationType = ValidationTypes.GreatThan,
                                                   ValueToCompare = gtv.ValueToCompare
                                               });
                                break;

                            case "LessThanOrEqualValidator":
                                LessThanOrEqualValidator ltoe = propertyValidator as LessThanOrEqualValidator;
                                result.Add(new ValidationConfig()
                                               {
                                                   ValidationType = ValidationTypes.LessThanOrEqualTo,
                                                   ValueToCompare = ltoe.MemberToCompare
                                               });
                                break;

                            case "RegularExpressionValidator":
                                RegularExpressionValidator rev = propertyValidator as RegularExpressionValidator;
                                result.Add(new ValidationConfig()
                                               {
                                                   ValidationType = ValidationTypes.RegularExpression,
                                                   Expression = rev.Expression
                                               });
                                break;
                        }
                    }
                }
            }

            return result;
        }
    }
}