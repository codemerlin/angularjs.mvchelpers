﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Angularjs.MvcHelpers.Mvc
{
    public static class ModelWrapper
    {
        public static ModelWrapper<TViewModel, TDataModel> Create<TViewModel, TDataModel>(string clientModelProp, TViewModel viewModel,
                                                                                          TDataModel dataModel)
            where TViewModel : class, new()
            where TDataModel : class, new()
        {
            return new ModelWrapper<TViewModel, TDataModel>(clientModelProp, viewModel, dataModel);
        }
    }


    public class ModelWrapper<TViewModel>
        where TViewModel : class, new()
    {
        public ModelWrapper(string clientModelProp, TViewModel viewModel)
        {
            ClientModelProp = clientModelProp;
            ViewModel = viewModel;

        }

        public TViewModel ViewModel { get; private set; }

        public string ClientModelProp { get; set; }
    }

    public class ModelWrapper<TViewModel, TDataModel> : ModelWrapper<TViewModel>
        where TViewModel : class, new()
        where TDataModel : class ,new()
    {
        public ModelWrapper(string clientModelProp, TViewModel viewModel, TDataModel dataModel)
            : base(clientModelProp, viewModel)
        {

            DataModel = dataModel;
        }

        public TDataModel DataModel { get; private set; }

    }

}