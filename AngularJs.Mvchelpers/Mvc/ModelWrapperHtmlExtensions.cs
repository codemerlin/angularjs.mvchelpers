﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Angularjs.MvcHelpers.Converters;
using FluentValidation;
using FluentValidation.Attributes;
using FluentValidation.Internal;
using FluentValidation.Validators;

namespace Angularjs.MvcHelpers.Mvc
{
    //str - extensions for Html
    public static class ModelWrapperExtensions
    {
        public static MvcHtmlString TextBoxFor<TViewModel, TDataModel, TValue>(
             this HtmlHelper<ModelWrapper<TViewModel, TDataModel>> html,
             Expression<Func<TDataModel, TValue>> expression, object attributes = null)
            where TViewModel : class, new()
            where TDataModel : class, new()
        {
            return new MvcHtmlString(new ChainedTagBuilder("input")
                .Attribute("ng-model", html.ViewData.Model.ClientModelProp + "." + ExpressionHelper.GetExpressionText(expression))
                .MergeAttributes(attributes.ToDictionary())
                .MergeAttributes(ConverterFacade.Convert(expression, html.ViewData.Model.DataModel))
                .ToString());
        }



        public static MvcHtmlString DisplayFor<TViewModel, TDataModel, TValue>(
            this HtmlHelper<ModelWrapper<TViewModel, TDataModel>> html,
            Expression<Func<TDataModel, TValue>> expression, string tagname,
            object attributes = null)
            where TViewModel : class, new()
            where TDataModel : class, new()
        {
            return new MvcHtmlString(new ChainedTagBuilder(tagname)
                .SetHtml("{{" + html.ViewData.Model.ClientModelProp + "." + ExpressionHelper.GetExpressionText(expression) + "}}")
                .MergeAttributes(attributes.ToDictionary())
                .ToString());
        }

        public static MvcHtmlString Label<TViewModel, TDataModel, TValue>(
            this HtmlHelper<ModelWrapper<TViewModel, TDataModel>> html,
            Func<TViewModel, TValue> expression, string tagname = "span")
            where TViewModel : class, new()
            where TDataModel : class, new()
        {
            var evaluatedValue = expression(html.ViewData.Model.ViewModel).ToString();
            return new MvcHtmlString(new ChainedTagBuilder(tagname)
                .SetHtml(evaluatedValue)
                .ToString());
        }

        private static string ToHtmlAttributeString(this Object attributes)
        {
            if (attributes == null)
                return "";
            var props = attributes.GetType().GetProperties();
            var pairs = props.Select(x => string.Format(@"{0}=""{1}""", x.Name, x.GetValue(attributes, null))).ToArray();
            return string.Join(" ", pairs);
        }

        private static IDictionary<string, string> ToDictionary(this Object attributes)
        {
            if (attributes == null)
                return new Dictionary<string, string>();
            return attributes
                .GetType()
                .GetProperties()
                .ToDictionary(propInfo => propInfo.Name, propInfo => propInfo.GetValue(attributes, null).ToString());
        }
    }


}