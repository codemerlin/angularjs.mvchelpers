﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation;
using FluentValidation.Attributes;
using FluentValidation.Results;

namespace Angularjs.MvcHelpers.Mvc
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class ValidateInputsAttribute : ActionFilterAttribute
    {
        private static readonly IValidatorFactory ValidatorFactory = new AttributedValidatorFactory();

        public override void OnActionExecuting(ActionExecutingContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            var errors = new List<dynamic>();
            foreach (KeyValuePair<string, object> arg in actionContext.ActionParameters.Where(a => a.Value != null))
            {
                var argType = arg.Value.GetType();
                IValidator validator = ValidatorFactory.GetValidator(argType);
                if (validator != null)
                {
                    var validationResult = validator.Validate(arg.Value);
                    foreach (ValidationFailure error in validationResult.Errors)
                    {
                        errors.Add(new { Error = error, ElementId = argType.Name + "-" + error.PropertyName });
                    }
                }
            }
            if (errors.Any())
            {
                HttpContext.Current.Response.StatusCode = 400;

                actionContext.Result = new JsonResult()
                {
                    Data = errors
                };
            }
        }
    }
}