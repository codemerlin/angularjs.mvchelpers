using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation;
using FluentValidation.Attributes;
using FluentValidation.Results;

namespace Angularjs.MvcHelpers.Mvc
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        private static readonly IValidatorFactory ValidatorFactory = new AttributedValidatorFactory();

        public string ParameterNameToValidate { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ActionParameters.ContainsKey(ParameterNameToValidate))
            {
                var model = filterContext.ActionParameters[ParameterNameToValidate];


                IValidator val = ValidatorFactory.GetValidator(model.GetType());
                ValidationResult results = val.Validate(model);

                if (results.Errors.Count > 0)
                {
                    HttpContext.Current.Response.StatusCode = 400;

                    filterContext.Result = new JsonResult()
                                               {
                                                   Data = results.Errors.ToArray()
                                               };
                }
            }
        }
    }
}