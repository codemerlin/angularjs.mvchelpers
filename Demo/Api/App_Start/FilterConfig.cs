﻿using System.Web.Mvc;

namespace AngularJs.Mvchelpers.Demo.Api.App_Start
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}