﻿using System.Web.Mvc;

namespace AngularJs.Mvchelpers.Demo.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
