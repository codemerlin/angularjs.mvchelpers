﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularJs.Mvchelpers.Demo.Runtime;
using Angularjs.MvcHelpers.Api;

namespace AngularJs.Mvchelpers.Demo.Api.Controllers
{
    //[ValidateInputs]
    public class ItemController : ApiController
    {
        private static readonly List<ItemDataModel> ListItems = new List<ItemDataModel>()
                {
                    new ItemDataModel(){ Id = 1, Name = "Item 1", Quantity = 101, Price = 78.0}
                    ,new ItemDataModel(){ Id = 2, Name = "Item 2", Quantity = 102, Price = 89.0}
                    ,new ItemDataModel(){ Id = 3, Name = "Item 3", Quantity = 103, Price = 58.0}
                    ,new ItemDataModel(){ Id = 4, Name = "Item 4", Quantity = 104, Price = 68.0}
                    
                };
        
        public HttpResponseMessage Options()
        {
            var response = new HttpResponseMessage();
            response.StatusCode = HttpStatusCode.OK;
            return response;
        }
        // GET api/values
        public IEnumerable<ItemDataModel> Get()
        {
            return ListItems;
        }

        // GET api/values/5
        public ItemDataModel Get(int id)
        {
            return ListItems.FirstOrDefault(i => i.Id == id);
        }

        // POST api/values
        public void Post([FromBody]ItemDataModel value)
        {
            if(value.Id==0 || ListItems.Any(k=>k.Id==value.Id))
                throw new ApplicationException("Item exists, item with the same id is there");
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]ItemDataModel value)
        {
            if (!(ListItems.Any(k => k.Id == id)))
                throw new ApplicationException("Can't Update item, there is no item with that id");
            ListItems.Remove(ListItems.Find(k => k.Id == id));
            ListItems.Add(value);
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            if(!(ListItems.Any(k=>k.Id==id)))
                throw new ApplicationException("Can't Delete item, there is no item with that id");
        }

       
    }
}