﻿using System.ComponentModel;
using FluentValidation.Attributes;

namespace AngularJs.Mvchelpers.Demo.Runtime
{
    [Validator(typeof(ItemModelValidator))]
    public class ItemDataModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SelectedOption { get; set; }
    }

}
