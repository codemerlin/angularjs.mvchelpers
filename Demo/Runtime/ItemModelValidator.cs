using System.Text.RegularExpressions;
using FluentValidation;

namespace AngularJs.Mvchelpers.Demo.Runtime
{
    //str - create the validator for a given model
    public class ItemModelValidator : AbstractValidator<ItemDataModel>
    {
        public ItemModelValidator()
        {
            RuleFor(i => i.Quantity).GreaterThan(0).WithMessage("You must have 1 or more of these!");
            //RuleFor(i => i.Quantity).LessThanOrEqualTo(5).WithMessage("You can't have more than 5 of these");
            RuleFor(i => i.Name).NotNull().WithMessage("Not a valid entry");
            RuleFor(i => i.Name).Matches(new Regex(@"^[A-Z][a-z0-9_-]{3,19}$")).WithMessage(
                "Must start with a capital letter");
            RuleFor(i => i.SelectedOption).NotEmpty().WithMessage("You must select at least one option!");
        }
    }
}