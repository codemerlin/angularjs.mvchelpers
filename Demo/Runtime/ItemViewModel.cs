﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace AngularJs.Mvchelpers.Demo.Runtime
{
    public class ItemViewModel 
    {
        public string NameLabel { get; set; }
        public string PriceLabel { get; set; }
        public string DescriptionLabel { get; set; }
        public string QuantityLabel { get; set; }
        public List<SelectListItem> ItemType { get; set; }
    }
}
