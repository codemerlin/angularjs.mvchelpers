﻿using System.Collections.Generic;
using System.Web.Mvc;
using AngularJs.Mvchelpers.Demo.Runtime;
using Angularjs.MvcHelpers.Mvc;

namespace AngularJs.Mvchelpers.Demo.Web.Controllers
{
    [ValidateInputs]
    public class ItemController : Controller
    {
        public ActionResult Index()
        {
            return View(ModelWrapper.Create("DataModel", GetViewModel(), new ItemDataModel()));
        }

        //[HttpPost]
        //public JsonResult Index(ItemModel model)
        //{
        //    return Json(model);
        //}

        private ItemViewModel GetViewModel()
        {
            return new ItemViewModel
                {
                    DescriptionLabel = " Enter Description ::",
                    NameLabel = "Enter Name ::",
                    PriceLabel = "Enter Price ::",
                    QuantityLabel = "Enter Quantity ::",
                    ItemType = new List<SelectListItem>() {
                                        new SelectListItem(){Value = "1", Text = "Really Cool"},
                                        new SelectListItem(){Value = "2", Text = "Something else cool"},
                                        new SelectListItem(){Value = "3", Text = "This isn't that cool"}
                                        }
                };

        }

    }
}
