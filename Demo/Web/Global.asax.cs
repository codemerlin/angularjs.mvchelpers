﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using AngularJs.Mvchelpers.Demo.Web.App_Start;
using AngularJs.Mvchelpers.Demo.Web.Models;
using FluentValidation.Mvc;
using FluentValidation.Validators;
using StructureMap;

namespace AngularJs.Mvchelpers.Demo.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //str - add a registry for StructureMap
            ObjectFactory.Configure(cfg => cfg.AddRegistry(new ModelRegistry()));
            
            //str - get the validator factory
            var validationFactory = new StructureMapValidatorFactory();

            //str - to get FV to pass custom model validation bits to the client...
            FluentValidationModelValidatorProvider.Configure(x =>
                x.Add(typeof(LessThanOrEqualValidator), (metadata, context, rule, validator) =>
                    new LessThanOrEqualToFluentValidationPropertyValidator(metadata, context, rule, validator)));

            //str - tell MVC to use FV validation and pass in factory
            ModelValidatorProviders.Providers.Add(new FluentValidationModelValidatorProvider(validationFactory));
            DataAnnotationsModelValidatorProvider.AddImplicitRequiredAttributeForValueTypes = false;

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            
        }
    }
}