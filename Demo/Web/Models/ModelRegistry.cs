﻿using AngularJs.Mvchelpers.Demo.Runtime;
using FluentValidation;
using StructureMap.Configuration.DSL;

namespace AngularJs.Mvchelpers.Demo.Web.Models
{
    //str - add a structure map registry for our validators
    public class ModelRegistry : Registry
    {
        public ModelRegistry()
        {
            For<IValidator>().Singleton().Use<ItemModelValidator>().Named("ItemViewModel");
            For<IValidator>().Singleton().Use<ItemModelValidator>().Named("ItemModel");
        }
    }
}