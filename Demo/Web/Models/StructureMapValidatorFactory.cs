﻿using System;
using FluentValidation;
using StructureMap;

namespace AngularJs.Mvchelpers.Demo.Web.Models
{
    //str - add a structure map validator factory
    public class StructureMapValidatorFactory : ValidatorFactoryBase
    {
        public override IValidator CreateInstance(Type validatorType)
        {
            return ObjectFactory.TryGetInstance(validatorType) as IValidator;
        }
    }
}