﻿//str - the application

var sampleApp = angular.module('sampleApp', [])
    .directive('ngGreaterThan', function (comparison) {
        
        if(element.text > comparison) {
            alert('greater than');
        } else {
            alert('less than');
        }
    });

sampleApp.DoPost = function ($scope, $http) {
    //turn off all the validation for now
    $http.post('/item/index', { Model: $scope.ItemModel })
        .success(function (data, status, headers, config) {
            alert('success');
        })
        .error(function(data, status, headers, config) {
            if (data.Errors.length > 0) {
                $scope.Errors = data.Errors;
                //angular.forEach(data.Errors, function(errorItem) {
                //    var msgElementId = '#' + errorItem.ElementId + '-ValidationMessage';
                //    $(msgElementId).css('display', 'inline');
                //});

            }
        });
};
var serverUrls = serverUrls ||
{
    apiServiceUrl: ''
};
sampleApp.controller('SampleController', function ($scope, $http) {
    $scope.isViewLoading = true;
    $http.get(serverUrls.apiServiceUrl+'Item/1').success(function(data, status, headers, config) {
        $scope.isViewLoading = false;
        $scope.DataModel = data;
    }).error(function(data, status, headers, config) {
        if (data.Errors.length > 0) {
            $scope.Errors = data.Errors;
            //angular.forEach(data.Errors, function(errorItem) {
            //    var msgElementId = '#' + errorItem.ElementId + '-ValidationMessage';
            //    $(msgElementId).css('display', 'inline');
            //});

        }
    });
    $scope.addQuantity = function () {

        if ($scope.myForm.$valid) {
            sampleApp.DoPost($scope, $http);
        }
        //perform server side validation knowing that the form is invalid
        else {
            sampleApp.DoPost($scope, $http);
        }
    };
});

